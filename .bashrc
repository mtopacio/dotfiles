export OS=$(uname -v | cut -d' ' -f1 | cut -d'-' -f2)

if [ $OS = "Microsoft" ]; then
    echo "Windows"
    export HOME=/mnt/c/Users/$(whoami)
fi
