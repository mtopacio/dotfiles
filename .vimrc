" REMINDERS #####################################
"
"   :PlugInstall to install plugins
"   :PlugUpdate to update plugins
"   :PlugClean + delete plugin from .vimrc to uninstall
"
"   zf    create a fold
"   zo    open a fold
"   zc    close a fold
"   zd    delete a fold
"
" GENERICS ######################################

command! -nargs=0 Sw w !sudo tee % > /dev/null  " save as sudo :Sw
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview

set encoding=UTF-8
set number                  " add line numbers
set ttyfast                 " speed up scrolling
set shiftwidth=4
set softtabstop=4
set autoindent
set expandtab
set wrap                   
set hlsearch                " highlight search
set foldenable              " enalbe folds on start up
set foldmethod=indent       " tabs define folds
set foldlevel=2             " lines with 2+ tabs will be folded
syntax on                   " turn syntax highlighting on

" PLUGINS with Vundle ########################################

filetype plugin indent on   "for plugins to load correctly
call plug#begin('~/.vim/plugged')

Plug 'vim-airline/vim-airline'              " status bar
Plug 'vim-airline/vim-airline-themes'       " more themes for airline
Plug 'scrooloose/nerdtree'                  " file explorer
Plug 'ryanoasis/vim-devicons'               " icons for nerdtree
Plug 'alvan/vim-closetag'                   " autocloses html tags
Plug 'townk/vim-autoclose'                  " autcloses quotes, parenthese
Plug 'scrooloose/syntastic'                 " syntax checker
Plug 'nvie/vim-flake8'                      " python highlighting
Plug 'pearofducks/ansible-vim'              " Ansible highlighting
Plug 'yggdroot/indentline'                  " indentation lines 
Plug 'SirVer/ultisnips'                     " snippet engine
Plug 'honza/vim-snippets'                   " snippet library 
Plug 'rust-lang/rust.vim'                   " rust syntax
Plug 'farmergreg/vim-lastplace'             " open files to last edit

Plug 'junegunn/goyo.vim'                    " turns vim into a reader
" usage:
" :Goyo - toggle Goyo
" :Goyo [dimension] - turn on or resize
" :Goyo! - turn off Goyo

call plug#end()

" PLUGIN CUSTOMIZATIONS ########################     

let g:airline_theme = 'wombat'
let g:airline#extensions#ale#enabled = 1

let g:ale_sign_column_always = 1
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '--'

let g:closetag_filenames = '*.html, *.xhtml, *.phtml, *.vue'
let g:closetag_shortcut = '>'
let g:closetag_close_shortcut = '<leader>>'

let python_highlight_all = 1

let g:indentLine_defaultGroup = 'SpecialKey'
let g:indentLine_char = '|'

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<C-.>"
let g:UltiSnipsJumpBackwardTrigger="<C-,>"
let g:UltiSnipsEditSplit="veritcal"

" KEY MAPPINGS #################################

map <C-r> :!clear;python3 %<CR>
map <C-j> :!clear;node %<CR>
map <C-n> :NERDTreeToggle<CR>
map <C-g> :Goyo<CR>
