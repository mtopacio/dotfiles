# dotfiles
Public repository of my own dotfiles, which are another name for configuration files due to the prefix `.`. These are usually used for client customizations.

Additional Requirements:
+ ripgrep (file searches for FZF)
+ bat (syntax colors for FZF preview)

```bash
$ pacman -S ripgrep bat
````

Files included:
+ .vimrc
