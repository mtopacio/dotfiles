#!/usr/bin/env bash

# installation script for customization dotfiles
# default is to install this into either your home directory or .config

echo
echo Installation script for dotfiles and other personal configurations

CONFIG="~/.config"

declare -A DOTFILES=([".vimrc"]="$HOME")

# Sylings
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No color
BOLD=$(tput bold)
NORM=$(tput sgr0)

for DF in "${!DOTFILES[@]}"; do
	TARGET=${DOTFILES[$DF]}
        echo -n "Creating symbolic link for ${BOLD}${DF}${NORM}: " 
	if [[ -f "${TARGET}/${DF}" ]]; then 
	    echo -e "${RED}FILE/LINK ALREADY EXISTS${NC}"
        else
            ln -s $(realpath ${DF}) ${TARGET}/${DF}
	    if [[ -f "${TARGET}/${DF}" ]]; then
		    echo -e "${GREEN}SUCCESS${NC}"
	    else
		    echo -e "${RED}ERROR${NC}"
	    fi
        fi
done

# vim plugins
if [[ ! -f $HOME/.vim/autoload/plug.vim ]]; then
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim 
    vim -c ":PlugInstall" -c ":q" -c ":q"
fi




echo 
echo Script Complete
